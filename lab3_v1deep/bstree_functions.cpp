/*
 * bstree_functions.cpp
 *
 *  Created on: Jun 2, 2015
 *      Author: betatester
 */

#include "bstree_functions.hpp"
#include "char_vector.hpp"

// Хороводы с бубном вокруг дерева

CCPtrVector g_CCPtrVector;

void BSTreePushNodeKeyToVector( const BSTree::Node & _node ) {
	CCPtrVectorPushBack( g_CCPtrVector, _node.m_key );
}

void BSTreeConvert( const BSTree & _tree, CCPtrVector & _vector ) {
	g_CCPtrVector = _vector;
	BSTreeSymmetricWalk( _tree, *BSTreePushNodeKeyToVector );
	_vector = g_CCPtrVector;
}

void BSTreeRead( BSTree & _tree, std::istream & _stream ) {
	CharVector buffer;
	CharVectorInit( buffer );
	while( ! _stream.eof() ) {
		CharVectorGetUntil( buffer, _stream, "\n" );
		BSTreeInsertKey( _tree, buffer.m_pData );
		CharVectorClear( buffer );
	}
	CharVectorDestroy( buffer );
}

void BSTreePrint( const BSTree & _tree, std::ostream & _stream ) {
	CCPtrVector resultVector;
	CCPtrVectorInit( resultVector );

	BSTreeConvert( _tree, resultVector );

	for (int i = 0; i < resultVector.m_nUsed; i++)
		_stream << resultVector.m_pData[i] << "\n";

	CCPtrVectorDestroy( resultVector );
}
