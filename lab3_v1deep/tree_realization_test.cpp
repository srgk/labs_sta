/*
 * tree_realization_test.cpp
 *
 *  Created on: Jun 1, 2015
 *      Author: betatester
 */

#include "tree_realization_test.hpp"
#include <cassert>
#include <string.h>
#include <iostream>

bool g_isEqual = true;
const char * g_testKeyValues[] = {
		"aaaaaa", "bbbbbb", "cccccc", "zzzzzz"
};
int g_funcCallsCounter = 0;

void IsAllNodeKeysEqual( const BSTree::Node & _node ) {
	if ( (! g_isEqual) && (g_funcCallsCounter > 0) )
		return;

	if ( strcmp( _node.m_key, g_testKeyValues[ g_funcCallsCounter++ ]) != 0 )
		g_isEqual = false;
	else
		g_isEqual = true;

	//std::cout << ...
}

bool TreeRealizationTest() {
	BSTree * tree = BSTreeCreate();

	BSTreeInsertKey( *tree, g_testKeyValues[3] );
	BSTreeInsertKey( *tree, g_testKeyValues[1] );
	BSTreeInsertKey( *tree, g_testKeyValues[1] );
	BSTreeInsertKey( *tree, g_testKeyValues[2] );
	BSTreeInsertKey( *tree, g_testKeyValues[0] );

	BSTreeSymmetricWalk( *tree, *IsAllNodeKeysEqual );

	BSTreeDestroy( tree );

	g_funcCallsCounter = 0;

	return ! g_isEqual;
}
