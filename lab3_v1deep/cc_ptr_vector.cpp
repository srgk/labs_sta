/*
 * cc_ptr_vector.cpp
 *
 *  Created on: Jun 2, 2015
 *      Author: betatester
 */

#include "cc_ptr_vector.hpp"

#include <cstring>
#include <iostream>
#include <cassert>

void CCPtrVectorInit ( CCPtrVector & _vector, int _allocatedSize )
{
    _vector.m_pData      = new const char * [ _allocatedSize ];
    _vector.m_nAllocated =  _allocatedSize;
    _vector.m_nUsed      = 0;
}


void CCPtrVectorDestroy ( CCPtrVector & _vector )
{
    delete[] _vector.m_pData;
}


void CCPtrVectorClear ( CCPtrVector & _vector )
{
    _vector.m_nUsed = 0;
}


bool CCPtrVectorIsEmpty ( const CCPtrVector & _vector )
{
    return _vector.m_nUsed == 0;
}


void CCPtrVectorGrow ( CCPtrVector & _vector )
{
    int nAllocatedNew = _vector.m_nAllocated * 2;
    const char ** pNewData = new const char * [ nAllocatedNew ];

    memcpy( pNewData, _vector.m_pData, sizeof( char * ) * _vector.m_nAllocated );

    delete[] _vector.m_pData;
    _vector.m_pData = pNewData;

    _vector.m_nAllocated = nAllocatedNew;
}


void CCPtrVectorPushBack ( CCPtrVector & _vector, const char * _data )
{
    if ( _vector.m_nUsed == _vector.m_nAllocated )
        CCPtrVectorGrow( _vector );

    _vector.m_pData[ _vector.m_nUsed++ ] = _data;
}



void CCPtrVectorPopBack ( CCPtrVector & _vector )
{
    assert( ! CCPtrVectorIsEmpty( _vector ) );

    -- _vector.m_nUsed;
}

void CCPtrVectorInsertAt ( CCPtrVector & _vector, int _position, const char * _data )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    int newUsed = _vector.m_nUsed + 1;
    if ( newUsed > _vector.m_nAllocated )
        CCPtrVectorGrow( _vector );

    for ( int i = _vector.m_nUsed; i > _position; i-- )
        _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

    _vector.m_pData[ _position ] = _data;

    _vector.m_nUsed = newUsed;
}


void CCPtrVectorDeleteAt ( CCPtrVector & _vector, int _position )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
        _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

    -- _vector.m_nUsed;
}
