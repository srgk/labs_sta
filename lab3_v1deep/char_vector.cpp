#include <iostream>
#include <string.h>
#include "char_vector.hpp"

void CharVectorInit (CharVector & _s, int _size) {
	_s.m_pData = new char[_size];
	_s.m_nUsed = 0;
	_s.m_nAllocated = _size;
}

void CharVectorDestroy (CharVector & _s) {
	delete [] _s.m_pData;
	_s.m_pData = nullptr;
}

void CharVectorClear (CharVector & _s) {
	_s.m_nUsed = 0;
}

void CharVectorGrow (CharVector & _s, int _sizeRequested) {
	int newSize;
	if (_sizeRequested < _s.m_nAllocated) {
		//По умолчанию увеличиваем длину строки на 10 символов
		newSize = _s.m_nAllocated + 10;
	}
	else {
		//Иначе округляем запрошенный размер до ближайшего большего десятка
		newSize = (_sizeRequested / 10 + 1) * 10;
	}
	CharVector newString;
	CharVectorInit(newString, newSize);
	strcpy (newString.m_pData, _s.m_pData );
	newString.m_nUsed = _s.m_nUsed;
	CharVectorDestroy(_s);
	_s.m_pData = newString.m_pData;
	_s.m_nAllocated = newSize;
}

void CharVectorPushBack ( CharVector & _s, const char _ch ) {
	if (_s.m_nUsed == _s.m_nAllocated)
		CharVectorGrow(_s, _s.m_nAllocated * 2);

	if ( _s.m_nUsed == 0)
		_s.m_pData[_s.m_nUsed ++] = _ch;

	else
		_s.m_pData[_s.m_nUsed - 1] = _ch;

	_s.m_pData[_s.m_nUsed ++] = '\0';
}

void CharVectorGetUntil (CharVector & _s, std::istream & _stream, const char * _terminator) {
	char buf = 'c';
	const int nOfTerminators = strlen (_terminator);
	while ( _stream.get(buf) ) {

		for (int i = 0; i < nOfTerminators; i++)
			if (_terminator[i] == buf)
				return;

		CharVectorPushBack (_s, buf);
	}

}

bool CharVectorIsDiffer (const CharVector & _one, const CharVector & _two) {
	if (_one.m_nUsed != _two.m_nUsed)
		return true;

	return ! static_cast < bool > ( strcmp ( _one.m_pData, _two.m_pData)  );
}

void CharVectorPrint (const CharVector & _s, std::ostream & _stream, char _separator) {
	_stream << _s.m_pData << _separator;
}

void CharVectorCopyAndReplace (CharVector & _dest, const CharVector & _source) {
	if ( _source.m_nUsed > _dest.m_nAllocated )
		CharVectorGrow ( _dest, _source.m_nUsed );

	strcpy ( _dest.m_pData, _source.m_pData );
	_dest.m_nUsed = _source.m_nUsed;
}

void CharVectorCopyAndReplace (CharVector & _dest, const char * _source) {
	int sourceStrLen = strlen(_source);

	if ( sourceStrLen > _dest.m_nAllocated )
		CharVectorGrow ( _dest, sourceStrLen + _dest.m_nAllocated );

	strcpy ( _dest.m_pData, _source );
	_dest.m_nUsed = sourceStrLen;
}
