/*
 * cc_ptr_vector.hpp
 *
 *  Created on: Jun 2, 2015
 *      Author: betatester
 */

#ifndef CC_PTR_VECTOR_HPP_
#define CC_PTR_VECTOR_HPP_

// CCPtrVector = ConstCharPointerVector

struct CCPtrVector {
	const char ** m_pData;
	int m_nUsed;
	int m_nAllocated;
};

void CCPtrVectorInit ( CCPtrVector & _vector, int _allocatedSize = 10 );

void CCPtrVectorDestroy ( CCPtrVector & _vector );

void CCPtrVectorClear ( CCPtrVector & _vector );

bool CCPtrVectorIsEmpty ( const CCPtrVector & _vector );

void CCPtrVectorPushBack ( CCPtrVector & _vector, const char * _data );

void CCPtrVectorPopBack ( CCPtrVector & _vector );

void CCPtrVectorInsertAt ( CCPtrVector & _vector, int _position, const char * _data );

void CCPtrVectorDeleteAt ( CCPtrVector & _vector, int _position );

#endif /* CC_PTR_VECTOR_HPP_ */
