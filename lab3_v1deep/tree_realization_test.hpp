/*
 * tree_realization_test.hpp
 *
 *  Created on: Jun 1, 2015
 *      Author: betatester
 */

#ifndef TREE_REALIZATION_TEST_HPP_
#define TREE_REALIZATION_TEST_HPP_

#include "bstree.hpp"

bool TreeRealizationTest();

#endif /* TREE_REALIZATION_TEST_HPP_ */
