#ifndef CHAR_VECTOR_H_
#define CHAR_VECTOR_H_

#include <iostream>

struct CharVector {
	int m_nAllocated = 0, m_nUsed = 0;
	char * m_pData = nullptr;
};

void CharVectorInit (CharVector & _s, int _size = 10);

void CharVectorDestroy (CharVector & _s);

void CharVectorClear (CharVector & _s);

void CharVectorGrow (CharVector & _s, int _sizeRequested = -1);

void CharVectorPushBack ( CharVector & _s, const char _ch );

void CharVectorGetUntil (CharVector & _s, std::istream & _stream = std::cin, const char * _terminator = "\n");

bool CharVectorIsDiffer (const CharVector & _one, const CharVector & _two);

void CharVectorPrint (const CharVector & _s, std::ostream & _stream = std::cout, char _separator = '\n');

void CharVectorCopyAndReplace (CharVector & _dest, const CharVector & _source);

void CharVectorCopyAndReplace (CharVector & _dest, const char * _source);

#endif /* CHAR_VECTOR_H_ */
