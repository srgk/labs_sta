#include "integer_vector.hpp"
#include <iostream>
#include <cassert>

void IntegerVectorReadTillZeroOrNegative ( IntegerVector & _vector, std::istream & _stream )
{
    while ( true )
    {
        int temp;
        _stream >> temp;
        if ( _stream && temp > 0 )
            IntegerVectorPushBack( _vector, temp );
        else
            break;
    }
}

int getValidP(IntegerVector & _vector, std::istream & _stream) {
	int P;
	_stream >> P;
	while ((P < 0) || (P > (_vector.m_nUsed / 2))) {
		std::cout << "\n invalid P value, try again: ";
		_stream >> P;
	}
	return P;
}

void IntegerVectorDeletePElements (IntegerVector & _vector) {
	int P = getValidP(_vector, std::cin);
	IntegerVectorDeleteAt(_vector, (_vector.m_nUsed - 1 - P));
	IntegerVectorDeleteAt(_vector, P);
}

int main() {
	IntegerVector vector;
	IntegerVectorInit(vector);
	IntegerVectorReadTillZeroOrNegative(vector, std::cin);
	IntegerVectorDeletePElements(vector);
	IntegerVectorPrint(vector, std::cout);
	return 0;
}

