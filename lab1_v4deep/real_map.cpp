#include "real_map.hpp"


#include <cassert>



// Создание объекта-отображения
RealMap * RealMapCreate ()
{
    // Создаем объект-отображение в динамической памяти
    RealMap * pMap = new RealMap;

    // Инициализируем оба внутренних вектора
    IntegerVectorInit( pMap->m_keys );
    RealVectorInit( pMap->m_values );

    // Возвращаем объект во внешний код
    return pMap;
}

// Уничтожение объекта-отображения
void RealMapDestroy ( RealMap * _pMap )
{
    // Освобождаем ресурсы внутренних векторов
    IntegerVectorDestroy( _pMap->m_keys );
    RealVectorDestroy( _pMap->m_values );

    // Удаляем сам объект-отображение
    delete _pMap;
}

// Очистка отображения
void RealMapClear ( RealMap & _map )
{
    // Очищаем оба внутренних вектора
    IntegerVectorClear( _map.m_keys );
    RealVectorClear( _map.m_values );
}

// Проверка отображения на пустоту
bool RealMapIsEmpty ( const RealMap & _map )
{
    // Отображение пусто, если пусты внутренние векторы.
    // Поскольку векторы модифицируются всегда одновременно,
    // достаточно проверить один из векторов на пустоту, например, вектор ключей
    return IntegerVectorIsEmpty( _map.m_keys );
}

// Вспомогательная функция для поиска позиции ключа в отображении
int RealMapFindKeyPosition ( const RealMap & _map, int _key )
{
    // Ищем позицию ключа в векторе ключей путем полного перебора
    for ( int i = 0; i < _map.m_keys.m_nUsed; i++ )
        if ( _map.m_keys.m_pData[ i ] == _key )
            // Позиция найдена
            return i;

    // Позиция не найдена
    return -1;
}

// Проверка отображения на содержание указанного ключа
bool RealMapHasKey ( const RealMap & _map, int _key )
{
    // Ключ есть в отображении, если поиск его позиции дает корректный ответ
    return RealMapFindKeyPosition( _map, _key ) != -1;
}

// Поиск значения по указанному ключу в отображении
int RealMapFind ( const RealMap & _map, int _key )
{
    // Ищем позицию ключа в векторе ключей
    int position = RealMapFindKeyPosition( _map, _key );

    // Ключ должен существовать!
    assert( position != -1 );

    // Используем найденную позицию для извлечения значения из вектора значений
    return _map.m_values.m_pData[ position ];
}

// Вставка пары ключ-значение в отображение
void RealMapInsertKey ( RealMap & _map, int _key, double _value )
{
    // Ищем позицию ключа в векторе ключей. Возможно такой ключ уже существует
    int position = RealMapFindKeyPosition( _map, _key );

    // Если такого ключа нет, добавляем ключ и значение в соответствующие вектора
    if ( position == - 1 )
    {
        IntegerVectorPushBack( _map.m_keys, _key );
        RealVectorPushBack( _map.m_values, _value );
    }
    else
       // В противном случае обновляем значение по существующему ключу
       _map.m_values.m_pData[ position ] = _value;
}

// Удаление элемента с указанным ключом из отображения
void RealMapRemoveKey ( RealMap & _map, int _key )
{
    // Ищем позицию ключа в векторе ключей
    int position = RealMapFindKeyPosition( _map, _key );

    // Ключ должен существовать!
    assert( position != -1 );

    // Одновременно удаляем ключ и значение из векторов в найденной позиции
    IntegerVectorDeleteAt( _map.m_keys, position );
    RealVectorDeleteAt( _map.m_values, position );
}
