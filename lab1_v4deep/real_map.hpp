#ifndef REAL_MAP_HPP_
#define REAL_MAP_HPP_

#include "integer_vector.hpp"
#include "real_vector.hpp"

// Форвардное объявление структуры-отображения //Отключено, выдает forward declaration error
//struct RealMap;

// Структура для отображения из двух отдельных векторов для ключей и значений
struct RealMap
{
    IntegerVector m_keys;
    RealVector m_values;
};

// Создание нового объекта-отображения
RealMap * RealMapCreate ();

// Уничтожение объекта-отображения
void RealMapDestroy ( RealMap * _pMap );

// Очистка отображения
void RealMapClear ( RealMap & _map );

// Проверка отображения на пустоту
bool RealMapIsEmpty ( const RealMap & _map );

// Проверка отображения на содержание указанного ключа
bool RealMapHasKey ( const RealMap & _map, int _key );

// Поиск значения по указанному ключу в отображении
int RealMapFind ( const RealMap & _map, int _key );

// Вставка пары ключ-значение в отображение
void RealMapInsertKey ( RealMap & _map, int _key, double _value );

// Удаление элемента с указанным ключом из отображения
void RealMapRemoveKey ( RealMap & _map, int _key );


#endif /* REAL_MAP_HPP_ */
