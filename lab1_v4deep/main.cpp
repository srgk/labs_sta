#include <fstream>

#include "number_stream_functions.hpp"
#include "real_map.hpp"
#include "print_message.hpp"
/*
В двух дисковых файлах, пути к котором передается первым и вторым аргументами
командной строки соответственно, содержатся записи о перечислениях на банковские
счета, по одной записи на строку в текстовом формате:

    <НОМЕР_ИЗ_ЦИФР> <СУММА_ДЕЙСТВИТЕЛЬНОЕ_ЧИСЛО>

В третьем аргументе командной строки содержится имя результирующего файла,
в который программа должна записать объединение двух первых, при этом платежи
на те же самые счета должны быть представлены однократно по сумме всех
объединенных. Программа должна сообщать об ошибочных ситуациях, таких как
 проблемы с открытием файлов или некорректный формат.

 */

/* Новая версия:
 * 1) уменьшено количество используемых отображений;
 * 2) все операции теперь проводятся через интерфейс отображения, а не в обход него.*/

void RealMapInsertKeyAndSumValues ( RealMap & _map, const int _key, double _value)
{
    // Проверяем наличие ключа
    if ( RealMapHasKey ( _map, _key ) ) {
    	// Если такой ключ уже записан - суммируем существующее значение с переданным
    	_value += RealMapFind ( _map, _key );
    }

    //Записываем новое значение
    RealMapInsertKey ( _map, _key, _value );
}

//Вывод отображения
void RealMapPrint (RealMap & _map, std::ostream & _stream) {
	for (int i = 0; i < _map.m_keys.m_nUsed; i++) {
		_stream << _map.m_keys.m_pData[i] << " ";
		_stream << _map.m_values.m_pData[i] << "\n";
	}
}

int BankRecordListFill (RealMap & _map1, std::istream & _stream, FILE_ID _fid = ONE_OF) {
	int key, lineReadCounter;
	double value;
	for (lineReadCounter = 1; ! _stream.eof() ; lineReadCounter++) {
		switch ( GetNextInt ( key, _stream ) ) {
		case -1:
			PrintMessage(WARN_EMPTY_LINE, std::cout, _fid, lineReadCounter);
			continue;

		case 1:
			PrintMessage(WARN_UNEXP_CHAR, std::cout, _fid, lineReadCounter);
		}

		if (key <= 0) {
			PrintMessage(WARN_WRONG_ID, std::cout, _fid, lineReadCounter);
			IgnoreUntilEOL( _stream );
			continue;
		}

		if ( ! GetNextReal(value, _stream) ) {
			PrintMessage(WARN_UNEXP_OEL, std::cout, _fid, lineReadCounter);
			value = 0;
		}

		if ( IgnoreUntilEOL ( _stream ) ) {
			PrintMessage(WARN_UNEXP_CHARS_UNTIL_EOL, std::cout, _fid, lineReadCounter);
		}
		RealMapInsertKeyAndSumValues(_map1, key, value);
	}
	return lineReadCounter;
}

int main (int _argc, char** _argv) {
	// Проверяем количество аргументов
	if (_argc < 4) {
		PrintMessage(ERR_FILE_NON_SPECIFIED, std::cout, static_cast<FILE_ID>(_argc - 1));
		return -1;
	}

	// Пытаемся открыть файлы на чтение и запись
	std::ifstream firstSourceFile(_argv[1]);
	std::ifstream secondSourceFile(_argv[2]);
	std::ofstream resultFile(_argv[3]);

	// Проверяем, удалось ли получить доступ к файлам и открыть их
	if ( ! firstSourceFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, FIRST_SOURCE_FILE);
		return -2;
	}

	if ( ! secondSourceFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, SECOND_SOURCE_FILE);
		return -2;
	}

	/* Эта проверка наверняка не работает. Но выходной файл все равно
	 будет создан, так что нужна ли она вообще? */
	if ( ! resultFile.is_open() ) {
		PrintMessage(ERR_BAD_FILE, std::cout, RESULT_FILE);
		return -2;
	}

	// Объявляем и инициализируем 3 отображения
	RealMap * resultList;
	resultList = RealMapCreate();

	// Копируем содержимое файлов 1 и 2 в две отдельные АТД
	BankRecordListFill( * resultList, firstSourceFile, FIRST_SOURCE_FILE );
	BankRecordListFill( * resultList, secondSourceFile, SECOND_SOURCE_FILE);

	// ...и закрываем файлы
	firstSourceFile.close();
	secondSourceFile.close();

	// Сохраняем содержимое конечного отображения в файл _resultFile,...
	RealMapPrint (* resultList, resultFile);

	// ...закрываем файл...
	resultFile.close();

	// ... и освобождлаем память
	RealMapDestroy( resultList );
	return 0;
}
