/*
 * time_range.cpp
 *
 *  Created on: May 4, 2015
 *      Author: betatester
 */

#include "time_range.hpp"
#include <cassert>
#include <iomanip>

//Определеляем, находился ли посетитель в ресторане в промежутке от _range.m_start до _range.m_end _Включительно_.
//UPD: Применимо не только для постетителей и ресторанов, кстати.
bool IsProcessPresentInTimeRange (const TimeRange & _visitData, const AbsoluteMinuteRange _range) {
	AbsoluteMinuteRange rangeInMinutes;
	//Конвертируем имеющиеся данные в минуты, отсчет минут ведется с 00:00
	rangeInMinutes.m_start = _visitData.m_startHour * MINUTES_IN_HOUR + _visitData.m_startMinute;
	rangeInMinutes.m_end = _visitData.m_finishHour * MINUTES_IN_HOUR + _visitData.m_finishMinute;

	/*
	 * Учитываем событие только в переделах одного дня, поэтому если событие началось в
	 *  один день, а закончилось в другой - считаем, что время окончания события равно 23:59
	 */
	if (rangeInMinutes.m_start > rangeInMinutes.m_end)
		rangeInMinutes.m_end = MINUTES_IN_HOUR * HOURS_IN_DAY - 1;

	//Ну и самое главное: определяем, длилось ли событие в указанный промежуток:
	if ( (rangeInMinutes.m_start >= _range.m_start) && (rangeInMinutes.m_start <= _range.m_end) )
		return true;

	if ( (rangeInMinutes.m_end >= _range.m_start) && (rangeInMinutes.m_end <= _range.m_end) )
		return true;

	if ( (rangeInMinutes.m_start < _range.m_start) && (rangeInMinutes.m_end > _range.m_end) )
		return true;

	return false;
}

void PrintVisitTime (TimeRange & _visitData, std::ostream & _stream, const char _sep) {
	 _stream << std::setfill('0') << std::setw(2) << _visitData.m_startHour << ":" << std::setw(2) << _visitData.m_startMinute << _sep
			 << std::setw(2) << _visitData.m_finishHour << ":"  << std::setw(2) << _visitData.m_finishMinute;
}

void ConvertDayPeriodIDtoTime (const int _periodID, TimeRange & _time, const int _nOfPeriodsInDay) {
	const double hoursInOnePeriod = static_cast<double>(HOURS_IN_DAY) / _nOfPeriodsInDay;

	double tempHour = (_periodID - 1) * hoursInOnePeriod;
	_time.m_startHour = static_cast<short>(tempHour);
	_time.m_startMinute = static_cast<short>((tempHour - _time.m_startHour) * MINUTES_IN_HOUR);

	tempHour = _periodID * hoursInOnePeriod;
	_time.m_finishHour = static_cast<short>(tempHour);
	_time.m_finishMinute = static_cast<short>((tempHour - _time.m_finishHour) * MINUTES_IN_HOUR);
}
