/*
 * vector_of_time_range_vectors.hpp
 *
 *  Created on: 6 мая 2015
 *      Author: strangeman
 */

#ifndef VECTOR_OF_TIME_RANGE_VECTORS_HPP_
#define VECTOR_OF_TIME_RANGE_VECTORS_HPP_

#include "time_range_vector.hpp"

// VOfTimeRangeVs = vector of [time range vector]'s
struct VOfTimeRangeVs {
	int m_nUsed, m_nAllocated = 0;
	TimeRangeVector * m_pData;
};

void VOfTimeRangeVsInit ( VOfTimeRangeVs & _vector, int _allocatedSize = 10 );

void VOfTimeRangeVsDestroy ( VOfTimeRangeVs & _vector );

void VOfTimeRangeVsClear ( VOfTimeRangeVs & _vector );

bool VOfTimeRangeVsIsEmpty ( const VOfTimeRangeVs & _vector );

void VOfTimeRangeVsGrow ( VOfTimeRangeVs & _vector );

void VOfTimeRangeVsPushBack ( VOfTimeRangeVs & _vector, const TimeRangeVector & _data );

void VOfTimeRangeVsPopBack ( VOfTimeRangeVs & _vector );

void VOfTimeRangeVsInsertAt ( VOfTimeRangeVs & _vector, int _position, const TimeRangeVector & _data );

//По умолчанию я не удаляю вложенные вектора во время вызова VOfTimeRangeVsDeleteAt или
// VOfTimeRangeVsPopBack, а помечаю их как не использованные и помещаю сразу за последним использованным вектором.
void VOfTimeRangeVsDeleteAt ( VOfTimeRangeVs & _vector, int _position );

//Эта функция будет принудительно освобождать память в куче от всех неиспользованных вложенных векторов
void VOfTimeRangeVsDestroyUnusedNested ( VOfTimeRangeVs & _vector );

void VOfTimeRangeVsDeleteAtNested ( VOfTimeRangeVs & _vector, int _indexOfNested, int _posInNested = -1 );

void VOfTimeRangeVsInsertAtNested ( VOfTimeRangeVs & _vector, const TimeRange & _data, int _indexOfNested, int _posInNested );

void VOfTimeRangeVsPushBackNested ( VOfTimeRangeVs & _vector, const TimeRange & _data, int _indexOfNested );

void VOfTimeRangeVsPopBackAtNested ( VOfTimeRangeVs & _vector, int _indexOfNested );

void VOfTimeRangeVsRead ( VOfTimeRangeVs & _vector, std::istream & _stream );

void VOfTimeRangeVsPrint ( const VOfTimeRangeVs & _vector, std::ostream & _stream, char _sep = '\n' );

#endif /* VECTOR_OF_TIME_RANGE_VECTORS_HPP_ */
