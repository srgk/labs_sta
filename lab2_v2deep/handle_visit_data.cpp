/*
 * handle_visit_data.cpp
 *
 *  Created on: 7 мая 2015
 *      Author: strangeman
 */

#include "handle_visit_data.hpp"
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iomanip>

void PushNewVisitToMap (TimeRangeMap & _map, const TimeRange & _visitTime) {
	const int minutesInPeriod = HOURS_IN_DAY / static_cast<double>( _map.m_nOfPeriods ) * MINUTES_IN_HOUR;

	//Заполняем вектор
	for (int i = 1; i <= _map.m_nOfPeriods; i++) {
		AbsoluteMinuteRange periodInAbsoluteMinutes;

		//Время начала i-го периода в минутах
		periodInAbsoluteMinutes.m_start = (i - 1) * minutesInPeriod;

		//Время завершения i-го периода в минутах
		periodInAbsoluteMinutes.m_end = periodInAbsoluteMinutes.m_start + minutesInPeriod - 1;

		//Собственно, заполнение значениями
		if ( IsProcessPresentInTimeRange (_visitTime, periodInAbsoluteMinutes) )
			TimeRangeMapInsertKey (_map, i, _visitTime);
	}
}

void FillRandomVisits (TimeRangeMap & _map, int _nOfVisits) {
	srand(time(NULL));
	TimeRange tempRange;
	for (int i = 0; i < _nOfVisits; i++) {
		tempRange.m_startHour = rand() % HOURS_IN_DAY;
		tempRange.m_finishHour = (tempRange.m_startHour + rand() % ( HOURS_IN_DAY + 1 - tempRange.m_startHour)) % HOURS_IN_DAY;
		tempRange.m_startMinute = rand() % MINUTES_IN_HOUR;
		tempRange.m_finishMinute = rand() % MINUTES_IN_HOUR;

		PushNewVisitToMap (_map, tempRange);
	}

}

int GetPeriodIDWithMaxAttendance (const TimeRangeMap & _map) {
	int temp, maxAttendance = 0, maxAttandanceID;
	for (int i = 1; i <= _map.m_nOfPeriods; i++) {
		temp = TimeRangeMapCountKey (_map, i);
		if (temp > maxAttendance) {
			maxAttendance = temp;
			maxAttandanceID = i;
		}
	}

	return maxAttandanceID;
}

void PrintVisitDataByPeriod (const TimeRangeMap & _map, const int _idOfPeriod, std::ostream & _stream, const char _sep ) {
	const TimeRangeVector temp = TimeRangeMapGet (_map, _idOfPeriod);
	if ( temp.m_nAllocated != 0 ) {
		TimeRangeVectorPrint (temp, _stream, _sep);
	}
	else
		_stream << "No visits\n";
}

void PrintVisitDataByAllPeriods (const TimeRangeMap & _map, std::ostream & _stream, const char _sep ) {
	TimeRange tempRange;
	for (int i = 1; i <= _map.m_nOfPeriods; i++) {
		ConvertDayPeriodIDtoTime (i, tempRange,  _map.m_nOfPeriods);
		_stream << "\nVisit(s) for period ";
		PrintVisitTime (tempRange, _stream, _sep);
		_stream << " are:\n";
		PrintVisitDataByPeriod (_map, i, _stream, _sep);
	}

}

void PrintVisitDataByMaxAttandance (const TimeRangeMap & _map, std::ostream & _stream, const char _sep ) {
	TimeRange tempRange;
	int tempID = GetPeriodIDWithMaxAttendance (_map);
	ConvertDayPeriodIDtoTime (tempID, tempRange,  _map.m_nOfPeriods);
	_stream << "\nVisit(s) for period with max attendance (";
	PrintVisitTime (tempRange, _stream, _sep);
	_stream << ") are:\n";
	PrintVisitDataByPeriod (_map, tempID, _stream, _sep);
}

