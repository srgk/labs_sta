/*
 * time_range_vector.cpp
 *
 *  Created on: 4 мая 2015
 *      Author: strangeman
 */

#include "time_range_vector.hpp"
#include <cstring>
#include <iostream>
#include <cassert>

void TimeRangeVectorInit ( TimeRangeVector & _vector, int _allocatedSize )
{
    _vector.m_pData      = new TimeRange[ _allocatedSize ];
    _vector.m_nAllocated =  _allocatedSize;
    _vector.m_nUsed      = 0;
}


void TimeRangeVectorDestroy ( TimeRangeVector & _vector )
{
    delete[] _vector.m_pData;
}


void TimeRangeVectorClear ( TimeRangeVector & _vector )
{
    _vector.m_nUsed = 0;
}


bool TimeRangeVectorIsEmpty ( const TimeRangeVector & _vector )
{
    return _vector.m_nUsed == 0;
}


void TimeRangeVectorGrow ( TimeRangeVector & _vector )
{
    int nAllocatedNew = _vector.m_nAllocated * 2;
    TimeRange * pNewData = new TimeRange[ nAllocatedNew ];

    memcpy( pNewData, _vector.m_pData, sizeof( TimeRange ) * _vector.m_nAllocated );

    delete[] _vector.m_pData;
    _vector.m_pData = pNewData;

    _vector.m_nAllocated = nAllocatedNew;
}


void TimeRangeVectorPushBack ( TimeRangeVector & _vector, const TimeRange & _data )
{
    if ( _vector.m_nUsed == _vector.m_nAllocated )
        TimeRangeVectorGrow( _vector );

    _vector.m_pData[ _vector.m_nUsed++ ] = _data;
}



void TimeRangeVectorPopBack ( TimeRangeVector & _vector )
{
    assert( ! TimeRangeVectorIsEmpty( _vector ) );

    -- _vector.m_nUsed;
}


void TimeRangeVectorRead ( TimeRangeVector & _vector, std::istream & _stream )
{
    while ( true )
    {
    	assert(false);
    	TimeRange temp;
        _stream >> temp.m_startHour >> temp.m_startMinute >> temp.m_finishHour >> temp.m_finishMinute;
        if ( _stream )
            TimeRangeVectorPushBack( _vector, temp );
        else
            break;
    }
}

void TimeRangeVectorPrint ( const TimeRangeVector & _vector, std::ostream & _stream, char _sep )
{
    for ( int i = 0; i < _vector.m_nUsed; i++ )
        _stream << _vector.m_pData[ i ].m_startHour << ":" << _vector.m_pData[ i ].m_startMinute << _sep
        << _vector.m_pData[ i ].m_finishHour << ":" << _vector.m_pData[ i ].m_finishMinute << "\n";
}


void TimeRangeVectorInsertAt ( TimeRangeVector & _vector, int _position, const TimeRange & _data )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    int newUsed = _vector.m_nUsed + 1;
    if ( newUsed > _vector.m_nAllocated )
        TimeRangeVectorGrow( _vector );

    for ( int i = _vector.m_nUsed; i > _position; i-- )
        _vector.m_pData[ i ] = _vector.m_pData[ i - 1];

    _vector.m_pData[ _position ] = _data;

    _vector.m_nUsed = newUsed;
}


void TimeRangeVectorDeleteAt ( TimeRangeVector & _vector, int _position )
{
    assert( _position >= 0 && _position < _vector.m_nUsed );

    for ( int i = _position + 1; i < _vector.m_nUsed; i++ )
        _vector.m_pData[ i - 1 ] = _vector.m_pData[ i ];

    -- _vector.m_nUsed;
}


