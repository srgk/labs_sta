/*
 * time_range_vector.hpp
 *
 *  Created on: 3 мая 2015
 *      Author: strangeman
 */

#ifndef CUSTOMER_VISIT_TIME_VECTOR_HPP_
#define CUSTOMER_VISIT_TIME_VECTOR_HPP_

#include <iostream>
#include "time_range.hpp"

struct TimeRangeVector
{
	TimeRange * m_pData;
    int m_nUsed;
    int m_nAllocated = 0;
};

void TimeRangeVectorInit ( TimeRangeVector & _vector, int _allocatedSize = 10 );

void TimeRangeVectorDestroy ( TimeRangeVector & _vector );

void TimeRangeVectorClear ( TimeRangeVector & _vector );

bool TimeRangeVectorIsEmpty ( const TimeRangeVector & _vector );

void TimeRangeVectorPushBack ( TimeRangeVector & _vector, const TimeRange & _data );

void TimeRangeVectorPopBack ( TimeRangeVector & _vector );

void TimeRangeVectorInsertAt ( TimeRangeVector & _vector, int _position, const TimeRange & _data );

void TimeRangeVectorDeleteAt ( TimeRangeVector & _vector, int _position );

void TimeRangeVectorRead ( TimeRangeVector & _vector, std::istream & _stream );

void TimeRangeVectorPrint ( const TimeRangeVector & _vector, std::ostream & _stream, char _sep = ' ' );


#endif /* CUSTOMER_VISIT_TIME_VECTOR_HPP_ */
